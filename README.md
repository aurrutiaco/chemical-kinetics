# Chemical Kinetics

Program to compute numerically the concentration of different species (in different chemical reactions) at different times. The program is based on the [Turns][1] book, specfically Example 4.3.

## Usage

Currently the program is only tested in Linux. To compile the program, run the following command:

```bash
python3 __init__.py
```

Dependencies are numpy and matplotlib.

## Limitations

The program can take multiple reactions, however the elementary forward and reverse rate coefficients are limited to the values shown in the __init__.py file. The program can be extended to take more reactions and rate coefficients, but this is not implemented yet.

## References
[1] Turns, S. R. (1996). Introduction to combustion (Vol. 287, p. 569). New York, NY, USA: McGraw-Hill Companies.