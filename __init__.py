import numpy as np
import chemie as ch
import numerics

from matplotlib import pyplot as plt

if __name__ == "__main__":
    
    C = ch.Chain()

    k_f = np.zeros((3,1))
    k_b = np.zeros((3,1))

    T = 800
    k_f[0] = 1.82e11*np.exp(-38370/T)
    k_f[1] = 1.8e7*T*np.exp(-4680/T)
    k_f[2] = 1.2e11*T*np.exp(-(4.51e8)/(8314*T))

    k_b[0] = 3.8e10*np.exp(-425/T)
    k_b[1] = 3.8e6*T*np.exp(-20820/T)
    k_b[2] = 1e11/T



    C.add( ch.Reaction( { "O": 1 , "N2" : 1}, {"NO":1 , "N":1 }, k_f[0], k_b[0] ) )
    C.add( ch.Reaction( { "N": 1 , "O2" : 1}, {"NO":1 , "O":1 }, k_f[1], k_b[1] ) )
    C.add( ch.Reaction( { "O2" : 1}, {"O":2 }, k_f[2], k_b[2] ) )
    C.set_stoichiometric_matrices()

    t = 0.001
    dt = 0.0000001
    nt = int(t/dt)
    X = np.zeros((5,nt))
    X[list(C.species).index("O2"),0] = 0.21*3*(101.325)/(8.314*2500)
    X[list(C.species).index("N2"),0] = 0.79*3*(101.325)/(8.314*2500)

    
    for i in range(1,X.shape[1]):
        X[:,i] =  numerics.newton(C.production_rates, X[:,i-1], dt)
        

    for i in range(5):
        plt.plot(X[i,:])

    plt.show()