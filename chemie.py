import numpy as np

class Reaction:

    def __init__(self, reactants : dict, products : dict , k_f : float, k_r : float):
        """
        It is assumed that the reaction is of the form:
        reactants -> products
        With forward and reverse rate constants k_f and k_r respectively.
        A correct input would be:
        reactants = { "A" : 1, "B" : 2 }
        products = { "C" : 1, "D" : 1 }
        k_f = 1.0
        k_r = 1.0
        For a reaction of the form:
        A + 2B -> C + D
        """
        self.products = products
        """Products of the reaction"""
        self.reactants = reactants
        """Reactants of the reaction"""
        self.k_r = k_r
        """Reverse rate constant"""
        self.k_f = k_f
        """Forward rate constant"""
        self.species = set( reactants.keys() ).union( set( products.keys() ) )
        """Set of species involved in the reaction. The set is constructed by taking the union of the keys of the reactants and products dictionaries so that the species are not repeated."""

    def __str__(self) -> str:
        return f"Reactants: {self.reactants} \nProducts:  {self.products}"

class Chain:

    def __init__(self):
        self.reactions = []
        """List of reactions in the chain"""
        self.species = set({})
        """Set of species involved in the chain"""

    def add(self, reaction : Reaction ):
        """
        Function to add a reaction to the chain.
        Parameters:
        ----------
        reaction : Reaction
            Reaction to be added to the chain.

        Returns:
        --------
        None
        """
        self.reactions.append( reaction )
        self.species = self.species.union( reaction.species )

    def __getitem__(self, index):
        """
        Get a reaction from the chain by index.
        Parameters:
        ----------
        index : int
            Index of the reaction to be retrieved.

        Returns:
        --------
        Reaction
            Reaction at the specified index.
        """
        return self.reactions[index]
    
    def set_stoichiometric_matrices(self):
        """
        Takes all reactions in the chain and constructs the stoichiometric matrices v_r and v_p based on the stoichiometric coefficients of the reactants and products of each reaction.
        """

        self.n_reactions = len(self.reactions)

        # reactants
        self.v_r = np.zeros((self.n_reactions, len(self.species) ))
        
        # products
        self.v_p = np.zeros((self.n_reactions, len(self.species) ))
        for j, species_j in enumerate(self.species):
            for i, reaction_i in enumerate(self.reactions):
        
                if species_j in reaction_i.reactants.keys():
                    self.v_r[i,j] = reaction_i.reactants[ species_j ]

                if species_j in reaction_i.products.keys():
                    self.v_p[i,j] = reaction_i.products[ species_j ]


    def rate_of_progress(self, X):
        """
        It calculates the rate of progress of each reaction i in the chain. The rate of progress is calculated as:
        $$ q_i = k_{f,i} \prod_{j=1}^{N_s} X_j^{v_{r,ij}} - k_{r,i} \prod_{j=1}^{N_s} X_j^{v_{p,ij}} $$
        Parameters:
        ----------
        X : numpy.ndarray
            Array of concentrations of the species involved in the chain.

        Returns:
        --------
        q : numpy.ndarray
            Array of the rate of progress of each reaction in the chain.
        """
        
        q = np.zeros(self.n_reactions)

        for i in range(self.n_reactions):
            q[i] = self.reactions[i].k_f * np.prod( X ** self.v_r[i] ) - self.reactions[i].k_r * np.prod( X ** self.v_p[i] )

        return q
    
    def production_rates(self, X):
        """
        It calculates the production rates of each species j in the chain. The production rate of each species is calculated as:
        $$ \omega_j = \sum_{i=1}^{N_r} v_{p,ij} q_i $$
        Parameters:
        ----------
        X : numpy.ndarray
            Array of concentrations of the species involved in the chain.
        
        Returns:
        --------
        omega : numpy.ndarray
            Array of the production rates of each species in the chain.
        """

        self.v = self.v_p - self.v_r
        q = self.rate_of_progress(X)
        omega = self.v.T.dot(q)

        return omega
