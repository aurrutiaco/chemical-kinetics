import numpy as np


def g( new_value,func,dt, prev_value):
    """
    Assuming a implicit euler scheme for an ODE of the form: dx/dt = f(x). We get the discretized equation: x_n+1 = x_n - f(x_n+1)*dt. This function returns the residual g of this equation which is g(x_n+1) = x_n+1 + f(x_n+1)*dt - x_n
    Parameters:
    -----------
    new_value: np.array
        The value of x_n+1
    func: function
        The function f(x)
    dt: float
        The timestep
    prev_value: np.array
        The value of x_n

    Returns:
    --------
    g: np.array
        The residual of the discretized equation
    """

    return new_value - func(new_value)*dt - prev_value


def jacobian(func, dt, initial_guess, prev_value):
    """
    It returns the jacobian matrix of the function g(x_n+1) = x_n+1 + f(x_n+1)*dt - x_n. The jacobian matrix is the matrix of partial derivatives of g with respect to x_i. The jacobian matrix is calculated using the upwind difference method.
    Parameters:
    -----------
    func: function
        The function f(x)
    dt: float
        The timestep
    initial_guess: np.array
        The initial guess for the root of the function g(x_n+1) = x_n+1 + f(x_n+1)*dt - x_n. This jacobian is being used in the newton numerical scheme to find the root of this function, that is why the initial guess is needed.
    prev_value: np.array
        The value of x_n.

    Returns:
    --------
    J: np.array
        The jacobian matrix of g(x_n+1) = x_n+1 + f(x_n+1)*dt - x_n using the initial guess as the value of x_n+1.
    """
    
    h = 1e-15
    J = np.zeros((len(initial_guess),len(initial_guess)))
    for i in range(len(initial_guess)):
        step_size = np.zeros(len(initial_guess)) # Create a vector of zeros
        step_size[i] = h    # Create a step size vector with a step in the i'th direction
        J[:,i] = ( g( initial_guess+step_size,func,dt, prev_value)-g(initial_guess,func,dt, prev_value ) ) / h # Upwind difference to find the partial derivative of g with respect to x_i and add it to the jacobian matrix
        

    return J


def newton(func, prev_value, dt):
    """
    Newton numerical scheme to find roots. In this case is the root of the function g(x_n+1) = x_n+1 + f(x_n+1)*dt - x_n. The root is the value of x_n+1 that satisfies this equation. The scheme is iterative and the initial guess is x_n+1 = 0.0001*np.ones(len(x_n)). The scheme is iterated until the norm of the difference between the updated guess and the previous guess is less than 1e-6.
    Parameters:
    -----------
    func: function
        The function f(x). For the particular case of the application, the function f(x) is the production rates of the species in the chain. Refer to `chemie.py` for more details.
    prev_value: np.array
        The value of x_n. For the particular case of the application, the value of x_n is the species concentrations at the previous timestep.
    dt: float
        The timestep

    Returns:
    --------
    new_value: np.array
        The value of x_n+1 that satisfies the equation g(x_n+1) = x_n+1 + f(x_n+1)*dt - x_n. For the particular case of the application, the value of x_n+1 is the species concentrations at the current timestep.
    """

    initial_guess = 0.0001*np.ones(len(prev_value))
    updated_guess = np.ones(len(prev_value))
    while np.linalg.norm( updated_guess - initial_guess ) > 1e-6:
        
        J_x = jacobian( func , dt, initial_guess, prev_value)
        f_x = g( initial_guess, func, dt, prev_value)
        updated_guess = initial_guess - np.linalg.inv(J_x).dot(f_x)
        initial_guess = updated_guess

    new_value = updated_guess
    return new_value
